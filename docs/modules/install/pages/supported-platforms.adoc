= Supported Platforms and System Requirements
:navtitle: Supported Platforms
:test-count: over 1650
:test-coverage: 99%
// URLs
:url-issues: {url-repo}/issues

Antora runs on Linux, macOS, and Windows.
Thanks to this broad platform support, Antora is also used on many cloud platforms and continuous integration and delivery servers.

== Platforms

Our general policy is to align Antora's support of a platform version with the upstream project's lifecycle schedule for that version.
When a platform version reaches the end of active support by its maintainers or its end of life (EOL) -- which ever comes first--Antora no longer supports it.

// When we have specific notes about a platform (tweaks and/or links to bugs) add a 3rd column to this table labeled "Good to Know"
[cols="20,25a",width="50%"]
|===
|Supported Platform |Supported Version(s)

|Alpine Linux
|&#8805; 3.11

|Debian
|&#8805; 10.10

|Fedora
|&#8805; 33

|Ubuntu
|[%hardbreaks]
18.04 LTS
20.04 LTS

|macOS
|[%hardbreaks]
Mojave
Catalina
Big Sur

|Microsoft
|[%hardbreaks]
Windows 10
Windows Server 2016
Windows Server 2019

|Chrome
|Latest stable version

|Firefox
|Latest stable version

|MS Edge
|Latest stable version

|Node.js
|[%hardbreaks]
&#8805; 10.24.0
12
14
16
|===

.About the Antora Test Suite
****
At last count, Antora had {test-count} tests that cover {test-coverage} of the lines.
You could say we're just a teeny-tiny bit obsessed with writing high quality tests and improving test coverage.
****

=== Cloud platforms

Antora should operate as designed on common cloud platforms.
How you provision your cloud instance depends on your workload requirements and remote git repository sizes.
If you're running Antora on a cloud platform, we would love to hear about your experience (open a {url-issues}[new issue^] or join us in Antora's {url-chat}[community chat^]).

== Hardware recommendations

Your hardware requirements will depend primarily on the size of the remote git repositories your Antora pipeline fetches content from when it is generating your site.

The first time Antora runs, it fetches and clones all of the remote git repositories specified in a playbook.
The speed of this operation is dictated by the size of the remote repositories, environment input/output parameters, and network performance.
After the initial run, Antora caches the repositories locally.
On subsequent runs, Antora only reconnects to the remote repositories if the xref:playbook:runtime-fetch.adoc[fetch option] is enabled or the xref:playbook:runtime-cache-dir.adoc[cache folder] is removed.

[cols="2s,4",width="75%"]
|===
|RAM
|Memory requirements depend on the size of your Git repositories.
3GB should provide sufficient headroom.

|I/O
|Maximum throughput and minimum latency always make things nicer but aren't required.
|===

== Learn more

* xref:upgrade-antora.adoc[Upgrade to the latest Antora release].

* Install Antora for the first time on:

** xref:linux-requirements.adoc[Linux]
** xref:macos-requirements.adoc[macOS]
** xref:windows-requirements.adoc[Windows]

* Try the official xref:ROOT:antora-container.adoc[Antora Docker container].

////
OS Release schedule links

Alpine: https://alpinelinux.org/releases/
3.11 ends on 2021-11-01

Arch: https://www.archlinux.org/releng/releases/

Arch Linux releases once a month, with only the 3 most recent distros being officially available

Debian: https://www.debian.org/releases/

Fedora: https://fedoraproject.org/wiki/Releases
EOL of 33 is 2021-11-16

Ubuntu Linux: https://wiki.ubuntu.com/Releases

openSUSE: https://en.opensuse.org/Portal:42.3

Leap 42.3 is the current release
Checkout the Open Build Project: http://openbuildservice.org

Windows: https://en.wikipedia.org/wiki/Comparison_of_Microsoft_Windows_versions

Windows Server 2016: Mainstream support: Until January 11, 2022
Windows 10: TBD

MacOS https://en.wikipedia.org/wiki/MacOS_version_history
Mojave EOL Sept 2021

MS Edge: replaces IE 11 which is not being developed further; it is the default browser for Windows 10/Server 2016
////
